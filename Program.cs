using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraccion
{
    // clase abstracta
    abstract class Animal
    {
        // método abstracto (no tiene cuerpo)
        public abstract void animalSound();
        // método regular
        public void sleep()
        {
            Console.WriteLine("Zzz");
        }
    }

    // clase derivada (hereda de Animal)
    class Pig : Animal
    {
        public override void animalSound()
        {
            // el cuerpo de animalSound() está provisto aquí
            Console.WriteLine("The pig says: wee wee");
        }
    }
    // segunda clase derivada (hereda de Animal)
    class Dog : Animal
    {
        public override void animalSound()
        {
            // el cuerpo de animalSound() está provisto aquí
            Console.WriteLine("The dog says: wauf wauf");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Pig myPig = new Pig(); // crea el objeto Cerdo
            myPig.animalSound();  // llama el método abstracto
            myPig.sleep();  // llama el método regular

            Dog myDog = new Dog();
            myDog.animalSound();
            myDog.sleep();


            Console.Read();
        }
    }
}
